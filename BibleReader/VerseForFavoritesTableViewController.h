//
//  VerseForFavoritesTableViewController.h
//  BibleReader
//
//  Created by Student on 4/20/13.
//  Copyright (c) 2013 Harding University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VerseForFavoritesTableViewController : UITableViewController
@property (strong,nonatomic) NSArray *versesInChapter;
@property (strong,nonatomic) NSString *nameOfChapter;
@property (strong,nonatomic) NSString *nameOfBook;
@property (strong,nonatomic) NSMutableArray *checkedCells;
@property (nonatomic,strong) NSString *indexOfBook;
@end
