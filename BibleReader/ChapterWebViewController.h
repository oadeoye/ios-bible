//
//  ChapterWebViewController.h
//  BibleReader
//
//  Created by Student on 4/18/13.
//  Copyright (c) 2013 Harding University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChapterWebViewController : UIViewController<UIWebViewDelegate>
@property (strong,nonatomic) NSArray *versesInChapter;
@property (strong,nonatomic) NSString *nameOfChapter;
@property (strong,nonatomic) NSString *nameOfBook;
@property (nonatomic,strong) NSString *indexOfBook;


@end
