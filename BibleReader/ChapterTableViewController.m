//
//  ChapterTableViewController.m
//  BibleReader
//
//  Created by Student on 4/16/13.
//  Copyright (c) 2013 Harding University. All rights reserved.
//

#import "ChapterTableViewController.h"
#import "ChapterWebViewController.h"

@interface ChapterTableViewController ()

@end

@implementation ChapterTableViewController
@synthesize chaptersOfBook = _chaptersOfBook;
@synthesize nameOfBook = _nameOfBook;
@synthesize indexOfBook = _indexOfBook;

- (void)setChaptersOfBook:(NSArray *)chaptersOfBook {
    if (_chaptersOfBook != chaptersOfBook) {
        _chaptersOfBook = chaptersOfBook;
        [self.tableView reloadData];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"showChapterText"]){
        NSIndexPath *selectedRowIndexPath = self.tableView.indexPathForSelectedRow;
        //Let the ChapterTextView know which book its text came from
        [segue.destinationViewController setNameOfBook:self.nameOfBook];
        [segue.destinationViewController setIndexOfBook:self.indexOfBook];
        
        //And its chapter
        NSString *nameOfSelectedChapter = [[[self.chaptersOfBook objectAtIndex:selectedRowIndexPath.row] allKeys] objectAtIndex:0];
        [segue.destinationViewController setNameOfChapter:nameOfSelectedChapter];
        
        //For the title of the destinationViewController, separate the "Chapter" from the "number" with a space. (7 chars in the word "chapter")
        [segue.destinationViewController setTitle: [NSString stringWithFormat:@"%@ %@", [nameOfSelectedChapter substringToIndex:7], [nameOfSelectedChapter substringFromIndex:7]]]
        ;
        //Get the verses from the self.chaptersOfBook and send them with the segue to ChapterTextView
        NSArray *versesInChapter = [[self.chaptersOfBook objectAtIndex:selectedRowIndexPath.row] valueForKey:nameOfSelectedChapter];
        [segue.destinationViewController setVersesInChapter:versesInChapter];
        
        //NSLog(@"Name Of Book: %@ Name Of Chapter: %@ Verses: %@", self.nameOfBook, nameOfSelectedChapter, versesInChapter);
        
    }
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [self.chaptersOfBook count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"chapter";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    NSString *chapterName = [[[self.chaptersOfBook objectAtIndex:indexPath.row] allKeys] objectAtIndex:0];
    NSString *textOfFirstVerse = [[[[self.chaptersOfBook objectAtIndex:indexPath.row] valueForKey:chapterName] objectAtIndex:0] valueForKey:@"Verse1"];
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", [chapterName substringToIndex:7], [chapterName substringFromIndex:7]];
    cell.detailTextLabel.text = textOfFirstVerse;
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

@end
