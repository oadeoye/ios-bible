//
//  BibleReaderAppDelegate.h
//  BibleReader
//
//  Created by Student on 4/14/13.
//  Copyright (c) 2013 Harding University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BibleReaderAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSArray *bible;
@end
