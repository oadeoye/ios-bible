//
//  main.m
//  BibleReader
//
//  Created by Student on 4/14/13.
//  Copyright (c) 2013 Harding University. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BibleReaderAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BibleReaderAppDelegate class]));
    }
}
