//
//  ChapterWebViewController.m
//  BibleReader
//
//  Created by Student on 4/18/13.
//  Copyright (c) 2013 Harding University. All rights reserved.
//

#import "ChapterWebViewController.h"
#import "VerseForFavoritesTableViewController.h"

@interface ChapterWebViewController ()

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) NSString *verses;
@end

@implementation ChapterWebViewController
@synthesize versesInChapter = _versesInChapter; //Array of all verses
@synthesize nameOfChapter = _nameOfChapter;
@synthesize indexOfBook = _indexOfBook;
@synthesize nameOfBook = _nameOfBook;
@synthesize verses = _verses;                   //String with the text of all the verses
@synthesize webView = _webView;

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"goToAddFavorites"]) {
        [segue.destinationViewController setVersesInChapter:self.versesInChapter];
        [segue.destinationViewController setNameOfChapter: self.nameOfChapter];
        [segue.destinationViewController setIndexOfBook:self.indexOfBook];
        [segue.destinationViewController setNameOfBook:self.nameOfBook];
        
    }
}

- (void)prepareHtmlStringFromVerses {
    
    NSString *htmlStyles = @"<style> body { background-color: rgba(241,242,230,1.0); } span.number { font-family: Helvetica; font-size: 20; } span.verseText { font-family: Helvetica; font-size: 18; } p { line-height: 200% } </style>";
    NSString *headHtmlWithStyles = [NSString stringWithFormat:@"%@ %@ %@", @"<html><head>", htmlStyles, @"</head><body><p>"];
    NSString *numberHtmlSpanOpeningText = @"<b><span class =\"number\">";
    NSString *verseTextSpanOpeningText = @"<span class = \"verseText\">";
    
    
    NSString *allVerses = headHtmlWithStyles;
    NSString *verseText = @"";
    NSString *verseName = @"";
    
    for (int index = 0; index < [self.versesInChapter count]; index++ ) {
        NSDictionary *verse = [self.versesInChapter objectAtIndex:index];
        verseName = [[verse allKeys] objectAtIndex:0];
        verseText = [verse valueForKey:verseName];
        allVerses = [ allVerses stringByAppendingString:[NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@ ",numberHtmlSpanOpeningText,[verseName substringFromIndex:5], @"</span></b>", verseTextSpanOpeningText, verseText, @"</span>"]];
    }
    
    allVerses = [allVerses stringByAppendingString:@"</p></body></html>" ];    
    self.verses = allVerses;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self prepareHtmlStringFromVerses];
    [self.webView loadHTMLString:self.verses baseURL:nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
